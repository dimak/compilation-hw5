#ifndef __HW5_H
#define __HW5_H

#include <list>
#include <string>
#include <vector>

void errLex();
void errSyn();
void errSem();

typedef enum {
        OUTSPEC_ID,
        OUTSPEC_STRING,
} outspec_t;

typedef enum {
        LVALUE_ID,
        LVALUE_CELL,
} lvalue_t;

typedef enum {
        REL_EQ,
        REL_NE,
        REL_LE,
        REL_GE,
        REL_LT,
        REL_GT,
} rel_t;

typedef enum {
        LOGIC_AND,
        LOGIC_OR,
} logic_t;

typedef enum {
        ARITH_ADD,
        ARITH_SUB,
        ARITH_DIV,
        ARITH_MUL,
} arith_t;

typedef enum {
        TYPE_INT,
        TYPE_MATRIX,
} type_t;

struct mat_dim {
        unsigned int width;
        unsigned int height;
};

struct yystype {
        struct {
                bool immediate;
                int int_value;
                unsigned int location;
        } expr;

        struct {
                std::list<unsigned int> true_list;
                std::list<unsigned int> false_list;
        } boolean;

        struct {
                std::list<unsigned int> next_list;
        } stmt;

        struct lvalue {
                lvalue_t type;
                unsigned int location;
        } lvalue;

        struct {
                outspec_t type;
        } outspec;

        struct {
                std::string value;
        } string;

        struct {
                std::string value;
        } id;

        struct {
                type_t type;
        } type;

        struct {
                std::vector<unsigned int> locations;
        } matrix_row;

        struct {
                mat_dim dim;
                std::vector<std::vector<unsigned int> > locations;
        } matrix;

        struct {
                arith_t type;
        } arithop;

        struct {
                rel_t type;
        } relop;

        struct {
                logic_t type;
        } logicop;

        struct {
                unsigned int current;
                unsigned int expr;
                unsigned int total;
        } foreach;

        unsigned int next_ip;
};

#define YYSTYPE yystype


#endif
