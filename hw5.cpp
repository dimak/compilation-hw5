#include <iostream>
#include <cstdlib>
#include <cstdio>

#include "hw5.hpp"

using namespace std;

extern void yyparse();
extern FILE *yyin;

void errLex(){
    cout << "LEXICAL ERROR" << endl;
    exit(0);
}

void errSyn(){
    cout << "SYNTACTIC ERROR" << endl;
    exit(0);
}

void errSem(){
    cout << "SEMANTIC ERROR" << endl;
    exit(0);
}

int main(int argc, char **argv) {
#if 0
	if (argc < 2) {
		cout << argv[0] << ": fatal error: no input files" << endl;
		exit(1);
	}
	yyin = fopen(argv[1], "r");
#endif
	yyparse();
}
