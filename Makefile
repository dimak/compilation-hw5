CC=gcc
CXX=g++
LEX=flex
YACC=bison

CXXFLAGS=-g3 -O0

all: ex5.exe

ex5.exe: lex.yy.o hw5.tab.o hw5.o
	$(CXX) -o $@ $(LDFLAGS) $^

%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) $< -o $@

lex.yy.cpp: hw5.lex hw5.tab.cpp
	$(LEX) -o $@ $<

hw5.tab.cpp: hw5.ypp
	$(YACC) --graph -v -d $<

graph: hw5.tab.cpp
	dot -Tjpg hw5.dot -o hw5.jpg

clean:
	rm -f hw5 ex5.exe *.o hw5.tab.* lex.yy.* *.jpg *.dot *.output *.out

.PHONY:	all clean graph
