%{
#include "hw5.hpp"
#include "hw5.tab.hpp"
%}

%option yylineno
%option noyywrap

int             (0|[1-9][0-9]*)
string          (\"[^"]*\")
id              ([a-zA-Z]+)
rel_op_cmp      (<|>)
rel_op_cmp_clx  (<=|>=)
rel_op_eq       (==|!=)
logic_op_and    (&&)
logic_op_or     (\|\|)
arith_op_mul    ([*/])
arith_op_add    ([+\-])
comment         ("//"[^\n]*)

%%
int             { return INT; }
matrix          { return MATRIX; }
print           { return PRINT; }
read            { return READ; }
if              { return IF; }
else            { return ELSE; }
while           { return WHILE; }
foreach         { return FOREACH; }
in              { return IN; }
true            { return TRUE; }
false           { return FALSE; }
{int}           {
                        yylval.expr.int_value = atoi(yytext);
                        yylval.type.type = TYPE_INT;
                        return NUM;
                }
{string}        { yylval.string.value = yytext; return STRING; }
{id}            { yylval.id.value = yytext; return ID; }
{rel_op_cmp_clx} {
                        if (!strcmp(yytext, "<=")) {
                                yylval.relop.type = REL_LE;
                        } else if (!strcmp(yytext, ">=")) {
                                yylval.relop.type = REL_GE;
                        }
                        return REL_OP_CMP_COMPLEX;
                }
{rel_op_cmp}    {
                        if (!strcmp(yytext, "<")) {
                                yylval.relop.type = REL_LT;
                        } else if (!strcmp(yytext, ">")) {
                                yylval.relop.type = REL_GT;
                        }
                        return REL_OP_CMP;
                }
{rel_op_eq}     {
        if (!strcmp(yytext, "==")) {
                yylval.relop.type = REL_EQ;
        } else if (!strcmp(yytext, "!=")) {
                yylval.relop.type = REL_NE;
        }
        return REL_OP_EQ;
}
{logic_op_and}  { return LOGIC_OP_AND; }
{logic_op_or}   { return LOGIC_OP_OR; }
{arith_op_mul}  {
                        yylval.arithop.type = (yytext[0] == '*' ? ARITH_MUL : ARITH_DIV);
                        return ARITH_OP_MUL;
                }
{arith_op_add}  {
                        yylval.arithop.type = (yytext[0] == '+' ? ARITH_ADD : ARITH_SUB);
                        return ARITH_OP_ADD;
                }
=               { return ASSIGN; }
;               { return SC; }
,               { return CS; }
\(              { return LP; }
\)              { return RP; }
\{              { return LC; }
\}              { return RC; }
\[              { return LB; }
\]              { return RB; }
!               { return NOT; }
\.              { return STR_CON; }
{comment}       ;
[ \t\n\r]       { }
.               { errLex(); }

%%

